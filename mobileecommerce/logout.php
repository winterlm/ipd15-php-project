<?php

if (FALSE) {
    $app = new \Slim\Slim();
    $log = new Logger('main');
}

$app->get('/logout', function() use ($app) {
    $_SESSION['user'] = array();   // ----
    $app->render('logout.html.twig', array('sessionUser' => $_SESSION['user']));    // ------
});