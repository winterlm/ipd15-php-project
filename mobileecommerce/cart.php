<?php

if (FALSE) {
    $app = new \Slim\Slim();
    $log = new Logger('main');
}

$app->get('/api/cart', function() use ($app, $log) {
    if ($_SESSION['user']) {
        //$cartItems = getCartByUser();
        $valueList = totalByUser();


//        if (!$cartItems) {
//           $app->render('cart.html.twig', array('note' => 'no record found', 'valueList' => $valueList, 'sessionUser' => $_SESSION['user']));
//            return;
//        }
    } else {
        //$cartItems = getCartByGuest();
        $valueList = totalByGuest();
//        if (!$cartItems) {
//            //$app->render('cart.html.twig', array('note' => 'no record found', 'valueList' => $valueList, 'sessionUser' => $_SESSION['user']));
//            return;
//        }
    }
    echo json_encode($valueList, JSON_PRETTY_PRINT);
});

//$app->response()->header('content-type', 'application/json');
$app->get('/cart', function() use ($app, $log) {


    if ($_SESSION['user']) {
        $cartItems = getCartByUser();
        $valueList = totalByUser();


        if (!$cartItems) {
            $app->render('cart.html.twig', array('note' => 'no record found', 'valueList' => $valueList, 'sessionUser' => $_SESSION['user']));
            return;
        }
    } else {
        $cartItems = getCartByGuest();
        $valueList = totalByGuest();
        if (!$cartItems) {
            $app->render('cart.html.twig', array('note' => 'no record found', 'valueList' => $valueList, 'sessionUser' => $_SESSION['user']));
            return;
        }
    }
    // echo json_encode($valueList, JSON_PRETTY_PRINT);
    $app->render('cart.html.twig', array('cartItems' => $cartItems, 'valueList' => $valueList, 'sessionUser' => $_SESSION['user']));
});

$app->get('/cart/add/:id/:quantity', function($id, $quantity) use ($app, $log) {


    // verify submission
    $errorList = array();
    //Todo: ajax verification
    if (!(is_numeric($quantity)) || $quantity < 1) {
        array_push($errorList, "invalid quantity");
    }

    //
    if (!$errorList) {
        // state 2: successful submission
        if ($_SESSION['user']) {
            DB::insert('cartItems', array(
                'sessionId' => $_SESSION['user']['id'],
                'itemId' => $id,
                'quantity' => $quantity));

            $valueList = totalByUser();
            $cartItems = getCartByUser();
            if (!$cartItems) {
                $app->notFound();
                return;
            }
            $app->render('cart.html.twig', array('cartItems' => $cartItems, 'valueList' => $valueList, 'sessionUser' => $_SESSION['user']));
        } else {
            DB::insert('cartItems', array(
                'sessionId' => session_id(),
                'itemId' => $id,
                'quantity' => $quantity));
            $valueList = totalByGuest();
            $cartItems = getCartByGuest();
            if (!$cartItems) {
                $app->notFound();
                return;
            }
            $app->render('cart.html.twig', array('cartItems' => $cartItems, 'valueList' => $valueList, 'sessionUser' => $_SESSION['user']));
        }
    } else {
        // state 3: failed submission
        $app->render('index.html.twig', array(
            'errorList' => $errorList,
            'sessionUser' => $_SESSION['user']
        ));
    }
});

$app->put('/cart/update/:id', function($id) use ($app, $log) {
    $json = $app->request()->getBody();
    $quantity = json_decode($json, true); //true to force it to return associative array
    $value = $quantity['quantity'];
    // FIXME: validate $todo  
    $result = isCartItemValid($quantity);
    if ($result !== TRUE) {
        $log->err("PUT /cartItems failed: " . $result);
        $app->response()->status(400);
        echo json_encode($result);
        return;
    }


    $existing = DB::queryOneRow("SELECT * FROM cartItems WHERE id=%i", $id);
    if (!$existing) {
        $app->response()->status(404);
        echo json_encode("no result found");
    } else {


        DB::update('cartItems', array(
            'quantity' => $value), "id=%i", $id);
        echo json_encode(DB::affectedRows() == 1);
    }
});

$app->get('/cart/delete/:id', function($id) use ($app, $log) {
    if ($_SESSION['user']) {
        DB::delete('cartItems', "id=%i", $id);

        $valueList = totalByUser();
        $cartItems = getCartByUser();
        if (!$cartItems) {
            $app->render('cart.html.twig', array('note' => 'no record found', 'valueList' => $valueList, 'sessionUser' => $_SESSION['user']));
            return;
        }
    } else {
        DB::delete('cartItems', "id=%i", $id);

        $valueList = totalByGuest();
        $cartItems = getCartByGuest();
        if (!$cartItems) {
            $app->render('cart.html.twig', array('note' => 'no record found', 'valueList' => $valueList, 'sessionUser' => $_SESSION['user']));
            return;
        }
    }

    $app->render('cart.html.twig', array('cartItems' => $cartItems, 'valueList' => $valueList, 'sessionUser' => $_SESSION['user']));
});

function isCartItemValid($quantity) {
    if (is_null($quantity)) {
        return "JSON parsing failed";
    }
    if (count($quantity) != 1) {
        return "Invalid number of values";
    }
    if (!isset($quantity['quantity'])) {
        return "Required field missing";
    }

    return TRUE;
}
