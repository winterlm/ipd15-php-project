<?php

session_start();
require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

if (true) {
    DB::$user = 'mobile';
    DB::$dbName = 'mobile2';
    DB::$password = 'cxIOBbXCroUR7RNG';
    DB::$port = 3333;
//    DB::$port = 3306;
    DB::$host = 'localhost';
    DB::$encoding = 'utf8';
    DB::$error_handler = 'db_error_handler';
//    DB::debugMode();
} else {
    DB::$user = 'cp4907_emobile';
    DB::$dbName = 'cp4907_emobile';
    DB::$password = 'cxIOBbXCroUR7RNG';
    DB::$port = 3333;
    //DB::$port = 3306;
    DB::$encoding = 'utf8';
    DB::$error_handler = 'db_error_handler';
}

function db_error_handler($params) {
    global $app, $log;
    $log->error("SQL error: " . $params['error']);
    $log->error("SQL query: " . $params['query']);
    http_response_code(500);
    $app->render('fatal_error.html.twig');
    die; // don't want to keep going if a query broke
}

// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');


\Slim\Route::setDefaultConditions(array(
    'id' => '\d+' //same as '[0-9]+'
));

if (!isset($_SESSION['user'])) {
    $_SESSION['user'] = array();
}

//set global variabel count 
if ($_SESSION['user']) {
    $cartItems = getCartByUser();
} else {
    $cartItems = getCartByGuest();
}

$_SESSION['count'] = count($cartItems);

$twig = $app->view()->getEnvironment();
$twig->addGlobal('count', $_SESSION['count']);


$app->get('/products/:id/image', function($id) use ($app, $log) {
    $item = DB::queryFirstRow("SELECT photo,mimeType FROM items WHERE id=%i", $id);
    if (!$item) {
        $app->notFound();
        return;
    }
    $app->response()->header('content-type', $item['mimeType']);
    echo $item['photo'];
});
// first show on main page
$app->get('/debug', function() use ($app, $log) {
    echo 'hello';
    return;
});


// first show on main page
$app->get('/', function() use ($app, $log) {
    
    $productList = DB::query("SELECT * FROM items WHERE isFrontPage=%i", 1);
    if (!$productList) {
        $app->notFound();
        return;
    }
$log->debug("something wrong");
    if ($_SESSION['user']) {
        $cartItems = getCartByUser();
        $valueList = totalByUser();


        if (!$cartItems) {
            $app->render('index.html.twig', array('productList' => $productList, 'note' => 'no record found', 'valueList' => $valueList, 'sessionUser' => $_SESSION['user']));
            return;
        }
    } else {
        $cartItems = getCartByGuest();
        $valueList = totalByGuest();
        if (!$cartItems) {
            $app->render('index.html.twig', array('productList' => $productList, 'note' => 'no record found', 'valueList' => $valueList, 'sessionUser' => $_SESSION['user']));
            return;
        }
    }
    $app->render('index.html.twig', array('productList' => $productList, 'cartItems' => $cartItems, 'valueList' => $valueList, 'sessionUser' => $_SESSION['user']));
});


//click search button
if (isset($_POST['search'])) {
    $keyword = $app->request()->post('keyword');

    $productList = DB::query("SELECT * FROM items WHERE itemName=%s", $keyword);
    if (!$productList) {
        $app->notFound();
        return;
    }
    $app->render('category.html.twig', array('productList' => $productList));
    return;
}

//click see-more-details button
if (isset($_POST['see-more-details'])) {
    $id = $app->request()->post('id');
    $product = DB::queryFirstRow("SELECT id,itemName, description, price FROM  items  WHERE id=%i", $id);
    if (!$product) {
        $app->notFound();
        return;
    }
    $app->render('products.html.twig', array('p' => $product));
    return;
}


$app->get('/products/:id', function($id) use ($app, $log) {
    $product = DB::queryFirstRow("SELECT id,itemName, description, price FROM  items  WHERE id=%i", $id);
    if (!$product) {
        $app->notFound();
        return;
    }
    $app->render('products.html.twig', array('p' => $product));
});

function getCartByGuest() {
    $cartItems = DB::query("SELECT i.id itemId, c.id, i.itemName, i.description, i.price, c.quantity "
                    . "FROM cartItems as c, items as i where c.itemId = i.id && c.sessionId=%i", session_id());
    return $cartItems;
}

function getCartByUser() {
    $cartItems = DB::query("SELECT i.id itemId, c.id, i.itemName, i.description, i.price, c.quantity "
                    . "FROM cartItems as c, items as i where c.itemId = i.id && c.sessionId=%i", $_SESSION['user']['id']);
    return $cartItems;
}

function totalByGuest() {
    $subtotal = DB::queryFirstField(
                    "SELECT SUM(i.price * c.quantity) "
                    . " FROM cartItems as c, items as i"
                    . " WHERE c.sessionId=%s AND c.itemId = i.id", session_id());
    if (!$subtotal) {
        $valueList = array('shipping' => 0, 'taxes' => 0, 'total' => 0);
    } else {
        $shipping = 15.00;
        $taxes = ($subtotal + $shipping) * 0.15;
        $total = $subtotal + $shipping + $taxes;

        $valueList = array('shipping' => $shipping, 'taxes' => $taxes, 'total' => $total);
    }
    return $valueList;
}

function totalByUser() {
    $subtotal = DB::queryFirstField(
                    "SELECT SUM(i.price * c.quantity) "
                    . " FROM cartItems as c, items as i"
                    . " WHERE c.sessionId=%s AND c.itemId = i.id", $_SESSION['user']['id']);
    if (!$subtotal) {
        $valueList = array('shipping' => 0, 'taxes' => 0, 'total' => 0);
    } else {
        $shipping = 15.00;
        $taxes = ($subtotal + $shipping) * 0.15;
        $total = $subtotal + $shipping + $taxes;

        $valueList = array('shipping' => $shipping, 'taxes' => $taxes, 'total' => $total);
    }


    return $valueList;
}

require_once 'register.php';
require_once 'login.php';
require_once 'category.php';
require_once 'cart.php';
require_once 'admin.php';
require_once 'checkout.php';
require_once 'userprofile.php';
require_once 'logout.php';
require_once 'total.php';

$app->run();
