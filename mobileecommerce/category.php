<?php

if (FALSE) {
    $app = new \Slim\Slim();
    $log = new Logger('main');
}



$app->get('/category', function() use ($app) {
  
    $productList = DB::query("SELECT * FROM items ");
    if (!$productList) {
        $app->notFound();
        return;
    }
    $app->render('category.html.twig', array('productList' => $productList, 'sessionUser' => $_SESSION['user']));
});


$app->get('/category/:action', function($action) use ($app) {
    switch ($action) {
        case 'cellphone':
            $cellphoneList = DB::query("SELECT * FROM items WHERE category=%s", "Cell Phone");
            if (!$cellphoneList) {
                $app->notFound();
                return;
            }
            $app->render('category.html.twig', array('productList' => $cellphoneList, 'subcategory' => 'Cell Phone', 'sessionUser' => $_SESSION['user']));
            break;
        case 'ipad':
            $ipadList = DB::query("SELECT * FROM items WHERE category=%s", "iPad");
            if (!$ipadList) {
                $app->notFound();
                return;
            }
            $app->render('category.html.twig', array('productList' => $ipadList, 'subcategory' => 'iPad', 'sessionUser' => $_SESSION['user']));
            break;
        case 'tablet':
            $tabletList = DB::query("SELECT * FROM items WHERE category=%s", "Tablet");
            if (!$tabletList) {
                $app->notFound();
                return;
            }
            $app->render('category.html.twig', array('productList' => $tabletList, 'subcategory' => 'Tablet', 'sessionUser' => $_SESSION['user']));
            break;
        case 'accessories':
            $accessoriesList = DB::query("SELECT * FROM items WHERE category=%s", "Accessories");
            if (!$accessoriesList) {
                $app->notFound();
                return;
            }
            $app->render('category.html.twig', array('productList' => $accessoriesList, 'subcategory' => 'Accessories', 'sessionUser' => $_SESSION['user']));
            break;       
    }
});
