<?php

if (FALSE) {
    $app = new \Slim\Slim();
    $log = new Logger('main');
}

// 1: first show
$app->get('/register', function () use ($app, $log) {
    $app->render('register.html.twig');
});

// 2: submission
$app->post('/register', function () use ($app, $log) {
    // receiving submission
    $name = $app->request()->post('name');
    $email = $app->request()->post('email');
    $pass1 = $app->request()->post('pass1');
    $pass2 = $app->request()->post('pass2');
    $type = $app->request()->post('type');
    $accountNumber = $app->request()->post('accountNumber');

    $valueList = array('name' => $name, 'email' => $email, 'accountNumber' => $accountNumber);

    // 2.1: verify submission
    $errorList = array();
    if (strlen($name) < 2 || strlen($name) > 20) {
        array_push($errorList, "Name must be 2-20 characters long");
        unset($valueList['name']);
    }
    
    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
        array_push($errorList, "Please input the valid email");
        unset($valueList['email']);
    } else {
        $user = DB::queryFirstRow("SELECT ID FROM users WHERE email=%s", $email);
        if ($user) {
            array_push($errorList, "Email already registered");
            unset($valueList['email']);
        }
    }
    
    if (strlen($pass1) < 6 || strlen($pass1) > 20) {
        array_push($errorList, "Password must be at least 6-20 characters long");
    }
    if ($pass1 != $pass2) {
        array_push($errorList, "Both passwords must be identical.");
    }
    
    if ($type != "buyer" && $type != "seller") {
        array_push($errorList, "Account type must select.");
    }
    
    if (strlen($accountNumber) < 6 || strlen($accountNumber) > 15) {
        array_push($errorList, "Account Number must be 6-15 characters long");
        unset($valueList['accountNumber']);
    }

    // 3: Insert to DB users table
    if ($errorList) {
        $app->render('register.html.twig', array('v' => $valueList, 'errorList' => $errorList));
    } else {
        DB::insert('users', array('name' => $name, 'email' => $email, 'password' => $pass1, 'type' => $type, 'accountNumber' => $accountNumber));
//        $app->render('register_success.html.twig');
        $id = DB::insertId();
        $log->debug(sprintf("User %s created", $id));
        
        // TODO: transfer the session to register.twig
        $user = DB::queryFirstRow("SELECT * FROM users WHERE id=%d", $id);
        $_SESSION['user'] = $user;
        
        $app->render('register.html.twig', array('registerSuccess' => TRUE, 'sessionUser' => $_SESSION['user']['id'], 'sessionUserType' => $_SESSION['user']['type']));
    }
});