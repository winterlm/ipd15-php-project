<?php

if (FALSE) {
    $app = new \Slim\Slim();
    $log = new Logger('main');
}

//first show on checkout page
$app->get('/checkout', function() use ($app) {

    if ($_SESSION['user']) {
        $cartItems = getCartByUser();
        $valueList = totalByUser();


        if (!$cartItems) {
            $app->render('index.html.twig', array('note' => 'No order to check out', 'valueList' => $valueList));
            return;
        }
    } else {
        $cartItems = getCartByGuest();
        $valueList = totalByGuest();
        if (!$cartItems) {
            $app->render('index.html.twig', array('note' => 'No order to check out', 'valueList' => $valueList));
            return;
        }
    }

    $app->render('checkout.html.twig', array('cartItems' => $cartItems, 'valueList' => $valueList));
});

