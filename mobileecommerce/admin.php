<?php

if (FALSE) {
    $app = new \Slim\Slim();
    $log = new Logger('main');
}

// ==== /admin/products/add ====
$app->get('/admin/products/add', function() use ($app, $log) {
    if (!$_SESSION['user']) {   // check if user logged in
        $app->render('access_denied.html.twig');
        return;
    }
    $app->render('addedit_item.html.twig', array('sessionUser' => $_SESSION['user']));
});

if (isset($_POST['save'])) {    // Add new item
$app->post('/admin/products/add', function() use ($app, $log) {
    if (!$_SESSION['user']) {   // check if user logged in
        $app->render('access_denied.html.twig');
        return;
    }
    $itemName = $app->request()->post('itemName');
    $category = $app->request()->post('category');
    $description = $app->request()->post('description');
    $price = $app->request()->post('price');
    $quantity = $app->request()->post('quantity');
    $isFrontPage = $app->request()->post('isFrontPage');

    $valueList = array('itemName' => $itemName, 'category' => $category, 'description' => $description, 'price' => $price, 'quantity' => $quantity);
    //
    $errorList = array();
    if (strlen($itemName) < 2 || strlen($itemName) > 40) {
        array_push($errorList, "Item name must be 2-40 characters long");
        unset($valueList['itemName']);
    }

    if ($category != "Cell Phone" && $category != "iPad" && $category != "Tablet" && $category != "Accessories") {
        array_push($errorList, "Category must select.");
    }

    if (strlen($description) < 2 || strlen($description) > 100) {
        array_push($errorList, "Name must be 2-100 characters long");
        unset($valueList['description']);
    }

    if (!is_numeric($price) || $price < 0 || $price > 999999.99) {
        array_push($errorList, "Price must be 0-999999.99");
        unset($valueList['price']);
    }

    if (!is_numeric($quantity) || $quantity < 0 || $quantity > 99999) {
        array_push($errorList, "Quantity must be 0-99999");
        unset($valueList['quantity']);
    }

    //
    $image = $_FILES['photo'];
    $imageInfo = getimagesize($image['tmp_name']);
    if (!$imageInfo) {
        array_push($errorList, "File does not look like a valid image");
    } else {
        // never allow '..' in the file name
        if (strstr($image['name'], '..')) {
            array_push($errorList, "File name invalid");
        }
        // only allow select extensions
        $ext = strtolower(pathinfo($image['name'], PATHINFO_EXTENSION));
        if (!in_array($ext, array('jpg', 'jpeg', 'gif', 'png'))) {
            array_push($errorList, "File extension invalid");
        }
        // check mime-type submitted
        //$mimeType = $image['type']; // TODO: use getimagesize result mime-type instead
        $mimeType = $imageInfo['mime'];
        if (!in_array($mimeType, array('image/gif', 'image/jpeg', 'image/png'))) {
            array_push($errorList, "File type invalid");
        }
    }
    //
    if ($errorList) {
        $app->render('addedit_item.html.twig', array(
            'v' => $valueList, 'errorList' => $errorList));
    } else {
        // move_uploaded_file($image['tmp_name'], 'uploads/' . $image['name']);
        $imageData = file_get_contents($image['tmp_name']);
        DB::insert('items', array(
            'itemName' => $itemName,
            'sellerId' => $_SESSION['user']['id'],
            'category' => $category,
            'description' => $description,
            'photo' => $imageData,
            'mimeType' => $mimeType,
            'price' => $price,
            'quantity' => $quantity,
            "isFrontpage" => $isFrontPage
        ));
        $itemId = DB::insertId();
        $app->render('addedit_item.html.twig', array('addSuccess' => TRUE, 'itemId' => $itemId, 'sessionUser' => $_SESSION['user']));
    }
});
} elseif (isset($_POST['cancel'])) {    // Cancel and go to item list of current user
    $app->post('/admin/products/add', function() use ($app, $log) {
    if (!$_SESSION['user']) {   // check if user logged in
        $app->render('access_denied.html.twig');
        return;
    }
        $sellerId = $_SESSION['user']['id'];
        $itemList = DB::query("SELECT id, itemName, category, description, photo, mimeType, price, quantity, isFrontPage "
                        . "FROM items WHERE sellerId = $sellerId");
        $app->render('list_item.html.twig', array('itemList' => $itemList, 'sessionUser' => $_SESSION['user']));
    });
}

// ==== /admin/products/list ====
$app->get('/admin/products/list', function() use ($app, $log) {
    if (!$_SESSION['user']) {   // check if user logged in
        $app->render('access_denied.html.twig');
        return;
    }
    $sellerId = $_SESSION['user']['id'];
    $itemList = DB::query("SELECT id, itemName, category, description, photo, mimeType, price, quantity, isFrontPage "
                    . "FROM items WHERE sellerId = $sellerId");
    $app->render('list_item.html.twig', array('itemList' => $itemList, 'sessionUser' => $_SESSION['user']));

});

// -- display the photo stored in DB  --
$app->get('/admin/products/:id/photo', function($id) use ($app, $log) {
    $item = DB::queryFirstRow("SELECT photo,mimeType FROM items WHERE id=%i", $id);
    if (!$item) {
        $app->notFound();
        return;
    }
    $app->response()->header('content-type', $item['mimeType']);
    echo $item['photo'];
});

// ==== /admin/products/:id (edit / delete items) ====
$app->get('/admin/products/:id', function($id) use ($app, $log) {
    if (!$_SESSION['user']) {   // check if user logged in
        $app->render('access_denied.html.twig');
        return;
    }
    $item = DB::queryFirstRow("SELECT * FROM items WHERE id=%i", $id);
    if (!$item) {
        $app->notFound();
        return;
    }
    $app->render('addedit_item.html.twig', array('v' => $item, 'sessionUser' => $_SESSION['user']));
});

if (isset($_POST['save'])) {    // Edit item
    $app->post('/admin/products/:id', function($id) use ($app, $log) {
        if (!$_SESSION['user']) {   // check if user logged in
            $app->render('access_denied.html.twig');
            return;
        }
        $itemName = $app->request()->post('itemName');
        $category = $app->request()->post('category');
        $description = $app->request()->post('description');
        $price = $app->request()->post('price');
        $quantity = $app->request()->post('quantity');
        $isFrontPage = $app->request()->post('isFrontPage');

        $valueList = array('itemName' => $itemName, 'category' => $category, 'description' => $description, 'price' => $price, 'quantity' => $quantity);
        //
        $errorList = array();
        if (strlen($itemName) < 2 || strlen($itemName) > 40) {
            array_push($errorList, "Item name must be 2-40 characters long");
            unset($valueList['itemName']);
        }

        if ($category != "Cell Phone" && $category != "iPad" && $category != "Tablet" && $category != "Accessories") {
            array_push($errorList, "Category must select.");
        }

        if (strlen($description) < 2 || strlen($description) > 100) {
            array_push($errorList, "Name must be 2-100 characters long");
            unset($valueList['description']);
        }

        if (!is_numeric($price) || $price < 0 || $price > 999999.99) {
            array_push($errorList, "Price must be 0-999999.99");
            unset($valueList['price']);
        }

        if (!is_numeric($quantity) || $quantity < 0 || $quantity > 99999) {
            array_push($errorList, "Quantity must be 0-99999");
            unset($valueList['quantity']);
        }

        //
        $image = $_FILES['photo'];
        $imageInfo = getimagesize($image['tmp_name']);
        if (!$imageInfo) {
            array_push($errorList, "File does not look like a valid image");
        } else {
            // never allow '..' in the file name
            if (strstr($image['name'], '..')) {
                array_push($errorList, "File name invalid");
            }
            // only allow select extensions
            $ext = strtolower(pathinfo($image['name'], PATHINFO_EXTENSION));
            if (!in_array($ext, array('jpg', 'jpeg', 'gif', 'png'))) {
                array_push($errorList, "File extension invalid");
            }
            // check mime-type submitted
            //$mimeType = $image['type']; // TODO: use getimagesize result mime-type instead
            $mimeType = $imageInfo['mime'];
            if (!in_array($mimeType, array('image/gif', 'image/jpeg', 'image/png'))) {
                array_push($errorList, "File type invalid");
            }
        }
        //
        if ($errorList) {
            $app->render('addedit_item.html.twig', array(
                'v' => $valueList, 'errorList' => $errorList));
        } else {
            // move_uploaded_file($image['tmp_name'], 'uploads/' . $image['name']);
            $imageData = file_get_contents($image['tmp_name']);
            DB::update('items', array(
                'itemName' => $itemName,
//            'sellerId' => $_SESSION['user']['id'],
                'category' => $category,
                'description' => $description,
                'photo' => $imageData,
                'mimeType' => $mimeType,
                'price' => $price,
                'quantity' => $quantity,
                "isFrontpage" => $isFrontPage
                    ), "id=%d", $id);

            $app->render('addedit_item.html.twig', array('editSuccess' => TRUE, 'itemId' => $id, 'sessionUser' => $_SESSION['user']));
        }
    });
} elseif (isset($_POST['delete'])) {    // Delete item
    $app->post('/admin/products/:id', function($id) use ($app, $log) {
        DB::delete('items', "id=%d", $id);
        $sellerId = $_SESSION['user']['id'];
        $itemList = DB::query("SELECT id, itemName, category, description, photo, mimeType, price, quantity, isFrontPage "
                        . "FROM items WHERE sellerId = $sellerId");
        $app->render('list_item.html.twig', array('itemList' => $itemList, 'sessionUser' => $_SESSION['user']));
    });
}
