<?php

if (FALSE) {
    $app = new \Slim\Slim();
    $log = new Logger('main');
}


// State 1: first show on login page
$app->get('/login', function() use ($app, $log) {
    $app->render('login.html.twig');
});
// State 2: submission
$app->post('/login', function() use ($app, $log) {
    $email = $app->request()->post('email');
    $password = $app->request()->post('password');
   
    $valueList = array('email' => $email);
    // verify submission
//    $isLoginSuccessful = false;

    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    if ($user && ($user['password'] == $password)) {
        $isLoginSuccessful = true;
    }

    //
    if ($isLoginSuccessful) {
        // state 2: successful submission
        unset($user['password']);
        $_SESSION['user'] = $user;
        $app->render('login_success.html.twig', array(
            'v' => $valueList,
            'sessionUser' => $_SESSION['user']
        ));
    } else {
        // state 3: failed submission
        $app->render('login.html.twig', array(
            'v' => $valueList,
            'error' => true
        ));
    }
});


