<?php

session_start();
require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

if (FALSE) {
    DB::$user = 'mobile';
    DB::$dbName = 'mobile';
    DB::$password = 'cxIOBbXCroUR7RNG';
    DB::$port = 3333;
//    DB::$port = 3306;
    DB::$host = 'localhost';
    DB::$encoding = 'utf8';
    DB::$error_handler = 'db_error_handler';
} else {
    DB::$user = 'cp4907_mobile';
    DB::$dbName = 'cp4907_mobile';
    DB::$password = 'cxIOBbXCroUR7RNG';
//    DB::$port = 3333;
    DB::$port = 3306;
    DB::$encoding = 'utf8';
    DB::$error_handler = 'db_error_handler';
}

function db_error_handler($params) {
    global $app, $log;
    $log->error("SQL error: " . $params['error']);
    $log->error("SQL query: " . $params['query']);
    http_response_code(500);
    header('content-type: applicatin/json');
    echo json_encode("500 - internal error");
//    $app->render('fatal_error.html.twig');
    die; // don't want to keep going if a query broke
}

// Slim creation and setup
$app = new \Slim\Slim();

// ----- json
$app->response()->header('content-type', 'application/json');

\Slim\Route::setDefaultConditions(array(
    'id' => '\d+'
));
// =============================================================


$app->get('/userprofile/:id', function ($id) use ($app, $log){
    $user = DB::queryFirstRow("SELECT * FROM users WHERE id=%i", $id);
    echo json_encode($user, JSON_PRETTY_PRINT);
});

$app->get('/userprofile/transaction/:id', function ($id) use ($app, $log){
    $order = DB::query("SELECT * FROM orders WHERE sellerId=%i", $id);
    echo json_encode($order, JSON_PRETTY_PRINT);
});


$app->run();


