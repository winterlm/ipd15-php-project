<?php

if (FALSE) {
    $app = new \Slim\Slim();
    $log = new Logger('main');
}

$app->get('/userprofile', function() use ($app) {
    if (!$_SESSION['user']) {   // check if user logged in
        $app->render('access_denied.html.twig');
        return;
    }
    $userId = $_SESSION['user']['id'];

    $app->render('user_profile.html.twig', array('sessionUser' => $_SESSION['user']));
});